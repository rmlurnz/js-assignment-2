//This variables stores the amount of time as it would appear on the microwave display.
var timer = "";
//This varaible simply exists to start and stop the timer.
var activeTime;
//This variable ensures the timer can't be started multiple times and that it will clear the timer when stop is hit twice
var stopped = true;
// This varible holds state of microwave, on or Off
var powerState = false;

// This function is for inputing value entered by user.
function inputNumber(buttonElement) {
if (powerState == true) {
	if (timer.length < 4) {
    	timer += buttonElement;
    	var minutes = ("00" + Math.floor(Number(timer)/100)).substr(-2);
    	var seconds = ("00" + Number(timer)%100).substr(-2);
    	document.getElementById("timerScreen").value = minutes + ":" + seconds; 
  	} 
}
  	else {
       powerStateFalse();
    }
}

// Adding BackSpace function, to allow user correct any mistake while inputing.
function backSpace() {
	if (powerState==true) {
    	timer= timer.substr(0, timer.length - 1);
    	var minutes = ("00" + Math.floor(Number(timer)/100)).substr(-2);
    	var seconds = ("00" + Number(timer)%100).substr(-2);
    	document.getElementById("timerScreen").value = minutes + ":" + seconds; 
 	}
 	else {
       powerStateFalse();
    }
}


//This function starts the timer
function startTimer() {
	if (powerState == true) {  
    	if (stopped) {
        	stopped = false;
        	countDown();
        	activeTime = window.setInterval(countDown, 1000);
    	}
 	}
 	else {
      document.getElementById("timerScreen").innerHTML="Please valid Time";
	}
}

//This function stops and clears the timer
function stopTimer() {
 	if (powerState == true) {  
    	if (stopped) {
        	timer = "";
        	document.getElementById("timerScreen").value = "00:00";
    	}
    	else {
        	stopped = true;
        	window.clearInterval(activeTime);
    	} 
 	}
 	else {
       powerStateFalse();
 	}
}

//This function counts down the timer, automatically converting to base 60
function countDown() {
    var minutes = ("00" + Math.floor(Number(timer)/100)).substr(-2);
    var seconds = ("00" + Number(timer)%100).substr(-2);
    document.getElementById("timerScreen").value = minutes + ":" + seconds;
    if (seconds == "00") {
        if (minutes == "00") {
            timerEnd();
        }
        else {
            timer = Number(timer) - 41;
        }
    }
    else {
       timer = Number(timer) - 1; 
    }
}

// adding variables to hold audio/image file for when the timer is done - TBD
var endBeep = new Audio("pizzapop.m4a");
var endImage = new Image("microwavetruth.jpg");

// Adding a way to "blink" when timer is done, by accessing the value of timerScreen
//This function is called when the timer reaches 00:00, as of now it only stops the timer.
//Insert sound effects and fireworks here.

function timerEnd() {
    var finished = document.getElementById("timerScreen");
    setTimeout(function(){finished.value = ""}, 500);
    setTimeout(function(){finished.value = "YO' PIZZA POP IS DONE, SON"}, 1000);
    setTimeout(function(){finished.value = ""}, 1500);
    setTimeout(function(){finished.value = "YO' PIZZA POP IS DONE, SON"}, 2000);
    setTimeout(function(){finished.value = ""}, 2500);
    setTimeout(function(){finished.value = "YO' PIZZA POP IS DONE, SON"}, 3000);
    
    endBeep.play();

    timer = "";
    stopped = true;
    window.clearInterval(activeTime);
}

// Adding function for power button
function onOff(){
  	if (powerState == false) {
     	document.getElementById("microwave").style.display = "none";
     	powerState = true;
   	}
   	else {
    	powerState = false;
    	document.getElementById("microwave").style.display = "block";
    	powerStateFalse();
   	}
}

// Adding this function to eliminate duplication of this code in file.
function powerStateFalse(){
    document.getElementById("microwave").innerHTML = "Please turn the Power On";
    document.getElementById("microwave").style.border = "thick solid #FFFFFF";
    document.getElementById("microwave").style.color = "white";
    document.getElementById("microwave").style.backgroundColor = "black";
}