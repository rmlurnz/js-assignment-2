//We require four global variables, one to store the number the user is currently entering, one to
//store the number the user entered previously, one to store the type of operation that is being performed,
//and one to ensure the user does not enter a decimal point more than once. All values save the last will be stored as strings.
var n = "";
var prevN = "";
var op = "";
var decimal = false;

function clearCalc() {
	n = "";
	prevN = "";
	op = "";
	decimal = false;
	document.getElementById("calcScreen").value = "0";
}

function numberPress(num) {
	/*when buttons 0-9 are pressed*/
	if (num.value !== "." && n.length < 12) {
    	n += num.value;
		/*if "." is pressed when there is not a decimal present yet*/
  	}
  	else if (decimal == false && n.length < 12) {
    	n += num.value;
		/*change global variable decimal to true*/
		decimal = true;
		/*if "." is pressed when there is already a decimal*/
    }
	/*output value on screen*/
    document.getElementById("calcScreen").value = n;
}

function backSpace()
{
    n = n.substr(0, n.length - 1);
    document.getElementById("calcScreen").value = n;
}

function operatorPress(operator) {
	/*when prevN is empty, we make sure n is a value, then initialize prevN as n value and que the operator for next evaluate*/
	if (prevN == "") {
		if (n == "") {
			n = 0;
		}
		prevN = n;
		/*prevN is filled, we evalate the math so far*/
	}
	else {
		/*evaluate based on prevN, qued op, freshly inputed n, output the new prevN*/
		evaluateCalc();
		/*que up next operator */
	}
	/*n is set back to empty for new input*/
	decimal = false;
	op = operator.value;
	n = "";
}

function evaluateCalc() {

	if (n == "") {
		return;
	}
	if (n == "0" && op == "/") {
		document.getElementById("calcScreen").value = "UNDEFINED";
		setTimeout("clearCalc()", 2000);
	}
	else {
		prevN = eval(prevN + op + n);
		document.getElementById("calcScreen").value = prevN;
		decimal = false;
		n = "";
		op = "";
	}
}
